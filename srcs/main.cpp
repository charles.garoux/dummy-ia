#include "../includes/utils.hpp"

int main()
{
	// game_board	board(5, 10);
	// do
	// {
	// 	board.print_board();
	// 	board.test();
	// } while (1);

	int order, height, width, winning_length = 3;

	std::cin >> width>> height>> order >> winning_length;
	game_board board(height, width, winning_length);
	if (order == 2)
	{
		int input;
		std::cin >> input;
		board.put_stone(input);
	}
	else
	{
		int mid = width / 2;
		std::cout << mid << "\n" << std::flush;
		board.put_stone(mid);
		int input;
		std::cin >> input;
		board.put_stone(input);
	}
	while (1)
	{
		int max_score = -2147483648;
		int best_col = 0;
		bool found_best = false;
		for (int i = 0; i < width; ++i)
		{
			if (board.is_winning_move(i))
			{
				found_best = true;
				best_col = i;
				break;
			}
		}
		if (found_best == false)
		{
			for (int j = 0; j < width; ++j)
			{
				int i = board.searched_col[j];
				int score;
				if (board.is_losing_move(i))
					continue;
				board.put_stone(i);
				if ((score = negamax(board, board.get_stone_amounts() - board.get_board_size(), board.get_board_size() - board.get_stone_amounts())) > max_score)
				{
					max_score = score;
					best_col = i;
				}
				board.take_stone(i);
			}
		}

		std::cout << best_col << "\n"
				  << std::flush;
		board.put_stone(best_col);
		int input;
		std::cin >> input;
		board.put_stone(input);
	}
}
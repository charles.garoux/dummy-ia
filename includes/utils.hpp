#pragma once

#include "class/game_board.hpp"
#include <algorithm>
#include <unordered_map>
#include <queue>

int	negamax(game_board &board, int lower_bound, int upper_bound);

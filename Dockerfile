# Builder stage
FROM debian:10 as builder

RUN apt update && apt install -y make g++

WORKDIR /build

COPY . .

RUN make

# Runner stage
FROM debian:10-slim

WORKDIR /app

RUN adduser player && chown -R player /app
USER player

COPY --from=builder /build/player /app/player

CMD ./player
